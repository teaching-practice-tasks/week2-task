import unittest

from solution import get_anagrams


class TestSolution(unittest.TestCase):
    def test_1(self):
        data = ['listen', 'silent']
        self.assertEqual(data, get_anagrams(data))

    def test_2(self):
        data = ['list', 'race', 'triangle', 'earth', 'integral', 'goal', 'log', 'care']
        self.assertEqual(['race', 'triangle', 'integral', 'care'], get_anagrams(data))
        pass

    def test_3(self):
        data = ['KneE', 'kEeN', 'race', 'EartH', 'heART', 'eye']
        self.assertEqual(['knee', 'keen', 'earth', 'heart'], get_anagrams(data))
        pass

    def test_4(self):
        data = ['list', 'raCe', 'trianGle', 'earth', 'inTegral', 'triangle', 'rAcE', 'cAre']
        self.assertEqual(['race', 'triangle', 'integral', 'care'], get_anagrams(data))
        pass

    def test_5(self):
        data = ['list', 'raCe', 'trianGle', 'mom' 'earth', 'inTegral', 'cAre', 'MMO']
        self.assertEqual(['race', 'triangle', 'integral', 'care'], get_anagrams(data))
        pass
