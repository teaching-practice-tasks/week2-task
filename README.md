### Week 2: Anagrams vs Palindromes

Brief story (_you are not required to read_):

The anagrams and palindromes had been at war for as long as anyone could remember. The anagrams were a skilled and clever group, able to rearrange their letters to form new words and phrases. The palindromes, on the other hand, were unchangeable and unbending, their letters perfectly mirrored and symmetrical.

As the war raged on, it seemed that the palindromes had the upper hand. They were strong and resilient, and their perfect symmetry made them almost impossible to defeat. The anagrams knew that they had to come up with a new strategy if they were to stand a chance against the palindromes.

And so, the anagrams made a brave decision. They knew that the only way to defeat the palindromes was to sacrifice themselves, giving up their ability to rearrange their letters in order to become stronger and more resilient. But they also knew that this sacrifice came with a great price: if they were the last anagram standing and the palindromes were defeated, they would have to die.

One by one, the anagrams changed themselves, becoming palindromes themselves. And with their newfound strength and resilience, they were finally able to overcome the palindromes and emerge victorious in the war.

The anagrams knew that their victory had come at a great cost, but they were proud of what they had accomplished. They had proven that even the weakest and most flexible can find the strength to overcome their opponents, if they are willing to make the necessary sacrifices. And with their victory, the anagrams and palindromes were finally able to live in harmony, each respecting the other's unique strengths and abilities.

But as the anagrams looked around at their fallen comrades, they knew that they had paid a heavy price for their victory. They vowed to never forget those who had sacrificed themselves in the war, and to always remember the great cost of their victory.

## Requirements:
1. Should return anagrams in the same order as they come from input.
2. Anagrams should be returned in lower case and ignore case.
3. List of resulting anagrams must not include duplicated words, only the first occurrence and its first location.
4. Palindromes must be excluded from results and words are not considered anagrams if they match with palindrome.

Examples:
```
Input: cat god atc branch dog
Output: cat god atc dog
```
```
Input: cAt gOd aTC Dog
Output: cat god atc dog
```
```
Input: cat god cat dog cat
Output: cat god dog
```

```
Input: cat god ada cat dog cat dad
Output: cat god dog
```

You have to implement function `get_anagrams(words)` in `solution.py` file.

You may create additional functions, use loops and arrays.